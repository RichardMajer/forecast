<?php


class CitiesList
{
    public static function getCities()
    {
        $cities = [
            'Ziar nad Hronom',
            'Zvolen',
            'Zilina',
            'Banska Bystrica',
            'Bratislava',
            'Poprad',
        ];

        return $cities;
    }

}