<?php
//include __DIR__.'/Forecast.php';
//include __DIR__ . '/CitiesList.php';

class RandomWeatherGenerator
{
    public static function generateTemperature()
    {
        $temperature = rand( -20, 35 );
        return$temperature;
    }

    public static function generateWeatherIcon()
    {
        $weatherIcon = [
            'wi-day-sunny',
            'wi-day-fog',
            'wi-rain-wind',
            'wi-hail',
            'wi-thunderstorm',
            'wi-lightning',
        ];

        $random = rand(0, count($weatherIcon)-1);
        return $weatherIcon[$random];

    }

    public static function generateWind()
    {
        $wind = rand(0, 20);
        return $wind;
    }

    public static function generateRainfall()
    {
        $rainfall = rand(0, 100);
        return $rainfall;
    }

    public static function generateHumidity()
    {
        $humidity = rand(0, 100);
        return $humidity;
    }

}