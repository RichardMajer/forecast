<?php

//include __DIR__ . '/CitiesList.php';
include __DIR__ . '/RandomWeatherGenerator.php';

class Forecast
{
    private $city;
    private $date;
    private $temperature;
    private $weather;
    private $wind;
    private $rainfall;
    private $humidity;

    public function __construct($city, $date)
    {
        $this->city = $city;
        $this->date = $date;

        $db = new PDO('mysql:host=212.57.39.185;dbname=test_weatherwidg;charset=utf8mb4',
            'test_weatherwidg',
            'qf1ok99D'
        );
        $sql = "SELECT * FROM `forecasts` WHERE `city` = ? AND `date` = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute([
            $city,
            $date,
        ]);
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($data) {
            $data = reset($data);
            $this->temperature = intval($data['temperature']);
            $this->weather = $data['weather'];
            $this->rainfall = intval($data['rainfall_probability']);
            $this->humidity = intval($data['humidity']);
            $this->wind = intval($data['windspeed']);
        } else {
            $this->temperature = RandomWeatherGenerator::generateTemperature();
            $this->weather = RandomWeatherGenerator::generateWeatherIcon();
            $this->wind = RandomWeatherGenerator::generateWind();
            $this->rainfall = RandomWeatherGenerator::generateRainfall();
            $this->humidity = RandomWeatherGenerator::generateHumidity();
            $this->saveToDatabase();
        }
    }

    public function saveToDatabase()
    {
        $db = new PDO('mysql:host=212.57.39.185;dbname=test_weatherwidg;charset=utf8mb4',
            'test_weatherwidg',
            'qf1ok99D'
        );
        $sql = "INSERT INTO forecasts (city, date, temperature, weather, rainfall_probability, humidity, windspeed) VALUES (?,?,?,?,?,?,?)";
        $stmt = $db->prepare($sql);
        $result = $stmt->execute([
            $this->city,
            $this->date,
            $this->temperature,
            $this->weather,
            $this->rainfall,
            $this->humidity,
            $this->wind,
        ]);

    }


    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return int
     */
    public function getTemperature()
    {
        return $this->temperature;
    }

    /**
     * @return int
     */
    public function getWind()
    {
        return $this->wind;
    }

    /**
     * @return int
     */
    public function getRainfall()
    {
        return $this->rainfall;
    }

    /**
     * @return int
     */
    public function getHumidity()
    {
        return $this->humidity;
    }

    public function getDayName()
    {
        $dateTime = DateTime::createFromFormat('d.m.Y', $this->date);
        return $dateTime->format('D');
    }

    public  function getWeek()
    {
        ob_start();
        ?>

        <div class="week__box">
            <h1 class="week__box__day"><?= $this->getDayName() ?></h1>
            <i class="week__box__icon wi <?= $this->getWeather() ?> "></i>
            <p class="week__box__temperature"><?= $this->getTemperature() ?> &deg;C</p>
        </div>

        <?php
        $html = ob_get_clean();
        return $html;
    }

    public function getWeather()
    {
        return $this->weather;
    }
}