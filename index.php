<?php
include (__DIR__.'/assets/classes/Forecast.php');
include ( __DIR__ . '/assets/classes/CitiesList.php' );

?>

<!DOCTYPE html>
<html lang="sk">
<head>
    <title>WEATHER WIDGET</title>
    <link rel="shortcut icon" type="image/png" href="assets/img/23527-3-weather.png"/>
</head>
<body>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700&amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.css">

<?php

// VARIABLES
    $cities = CitiesList::getCities();
    $today = date('d.m.Y');
    $selectedCity = $cities[0];
    if (isset($_GET['city'])) {
        $selectedCity = $_GET['city'];
    }
?>

<body class="">
    <div class="container">
        <div class="today ">

            <div class="today__top">
                <form action="" class="today__top__selector">
                    <select name="city" id="" class="custom-select col-3">
                        <? foreach ($cities as $city) : ?>
                            <option value="<?= $city ?>" <?= ($city == $selectedCity) ? 'selected' : '' ?>><?= $city ?>
                        <? endforeach ?>
                    </select>
                    <input class="btn" type="submit" value="ok">
                </form>
                <?php
                $forecast1 = new Forecast($selectedCity, $today);
                ?>
                <p class="today__top__day "><?= date('l') ?></p>
                <p class="today__top__date "><?= $today ?></p>
            </div>

            <div class="today__more__details">
                <i class="today__more__details__icon wi <?= $forecast1->getWeather() ?>"> </i>
                <p class="today__more__details__temperature"><?= $forecast1->getTemperature() ?> &deg;C </p>
                <div class="today__more__details__group">
                    <p>Wind speed: <span><?= $forecast1->getWind() ?>  km/h </span> </p>
                    <p>Probability of precipitation: <span><?= $forecast1->getRainfall() ?> % </span></p>
                    <p>Humidity: <span><?= $forecast1->getHumidity() ?> % </span></p>
                </div>
            </div>

        </div>


        <div class="week">
            <?php
            $nextDay = DateTime::createFromFormat('d.m.Y', $today);
            for($i = 0; $i<6; $i++){
                $nextDay->modify('+1 day');
                $nextDayForecast = new Forecast($selectedCity, $nextDay->format('d.m.Y') );
                echo $nextDayForecast->getWeek();
            }
            ?>
        </div>

    </div>
</body>

</body>
</html>

